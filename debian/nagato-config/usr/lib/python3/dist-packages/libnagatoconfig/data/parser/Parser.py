
from libnagato4.Object import NagatoObject
from libnagatoconfig.Mikuru import PathEnumerator
from gi.repository import Gio
from gi.repository import GLib


class NagatoParser(NagatoObject):

    DIRECTORY = "do not apply me"

    def _check_file(self, paths):
        pass

    def _check_directory(self, file_info):
        if file_info.get_file_type() != Gio.FileType.DIRECTORY:
            return
        yuki_path_elements = [self.DIRECTORY, file_info.get_name()]
        yuki_directory = GLib.build_filenamev(yuki_path_elements)
        yuki_paths = PathEnumerator.get_all_names(yuki_directory)
        self._check_file(file_info, yuki_paths)

    def __init__(self, parent):
        self._parent = parent
        for yuki_file_info in PathEnumerator.yield_children(self.DIRECTORY):
            self._check_directory(yuki_file_info)
