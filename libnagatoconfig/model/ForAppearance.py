
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatoconfig.data.PrimaryDataPair import NagatoPrimaryDataPair
from libnagatoconfig.data.ForAppearance import NagatoData


class NagatoModel(NagatoObject):

    def set_current_setting(self, user_data):
        self._gtk.set_current_setting(user_data)

    def get_current_setting(self, key):
        return self._gtk[key]

    def get_list_store(self, key):
        yuki_list_store = Gtk.ListStore(str)
        for yuki_item in self._data_list[key]:
            yuki_list_store.append((yuki_item,))
        return yuki_list_store

    def save(self):
        self._gtk.save()
        self._raise("YUKI.N > header notify", ("info", "It's done."))

    def __init__(self, parent):
        self._parent = parent
        self._data_list = NagatoData(self)
        self._gtk = NagatoPrimaryDataPair(self)
