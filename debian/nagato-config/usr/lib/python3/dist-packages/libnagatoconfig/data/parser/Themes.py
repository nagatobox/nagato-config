
from libnagatoconfig.data.parser.Parser import NagatoParser


class NagatoThemes(NagatoParser):

    DIRECTORY = "/usr/share/themes"

    def _check_file(self, file_info, paths):
        if "index.theme" in paths:
            yuki_data = "themes", file_info.get_name()
            self._raise("YUKI.N > append data", yuki_data)
