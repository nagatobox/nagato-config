
from libnagato4.mainwindow.MainWindow import AbstractMainWindow
from libnagatoconfig.mainline.MainCloserLayer import NagatoMainCloserLayer


class NagatoMainWindow(AbstractMainWindow):

    def _on_initialize(self):
        NagatoMainCloserLayer(self)
