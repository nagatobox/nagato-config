
from libnagatoconfig.ui.button.Button import NagatoButton


class NagatoGoBack(NagatoButton):

    LABEL_TEXT = "Back"

    def _on_clicked(self, button):
        self._raise("YUKI.N > page move to", "portal")
