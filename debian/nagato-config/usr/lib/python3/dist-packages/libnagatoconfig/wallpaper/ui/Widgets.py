
from libnagatoconfig.wallpaper.ui.Spacer import NagatoSpacer
from libnagatoconfig.wallpaper.ui.ColorBox import NagatoColorBox
from libnagatoconfig.wallpaper.ui.ApplyBox import NagatoApplyBox
from libnagatoconfig.wallpaper.ui.DrawingArea import NagatoDrawingArea


class AsakuraWidgets:

    def __init__(self, parent):
        NagatoDrawingArea(parent)
        NagatoColorBox(parent)
        NagatoSpacer(parent)
        NagatoApplyBox(parent)
