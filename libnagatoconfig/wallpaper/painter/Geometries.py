
from gi.repository import Gdk
from libnagato4.Object import NagatoObject


class NagatoGeometries(NagatoObject):

    def _get_rate(self, size, monitor_rect):
        yuki_width_rate = size.width/monitor_rect.width
        yuki_height_rate = size.height/monitor_rect.height
        return min(yuki_width_rate, yuki_height_rate)

    def get_rate(self):
        yuki_size = self._enquiry("YUKI.N > allocated size")
        yuki_monitor_rect = self._monitor.get_geometry()
        return self._get_rate(yuki_size, yuki_monitor_rect)

    def get_geometry(self):
        yuki_size = self._enquiry("YUKI.N > allocated size")
        yuki_monitor_rect = self._monitor.get_geometry()
        yuki_rate = self._get_rate(yuki_size, yuki_monitor_rect)
        yuki_width = yuki_monitor_rect.width*yuki_rate
        yuki_x = (yuki_size.width - yuki_width)/2
        yuki_height = yuki_monitor_rect.height*yuki_rate
        yuki_y = (yuki_size.height - yuki_height)/2
        return yuki_x, yuki_y, yuki_width, yuki_height

    def __init__(self, parent):
        self._parent = parent
        yuki_display = Gdk.Display.get_default()
        self._monitor = yuki_display.get_monitor(0)
