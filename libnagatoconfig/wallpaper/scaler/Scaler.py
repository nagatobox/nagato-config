
from gi.repository import GdkPixbuf
from libnagato4.Object import NagatoObject

INTERP = GdkPixbuf.InterpType.BILINEAR


class AbstractScaler(NagatoObject):

    def _build(self, pixbuf, rate):
        yuki_width = pixbuf.get_width()*rate
        yuki_height = pixbuf.get_height()*rate
        return pixbuf.scale_simple(yuki_width, yuki_height, INTERP)

    def _get_scaled(self, pixbuf, geometry):
        raise NotImplementedError

    def get_scaled(self, pixbuf):
        yuki_geometry = self._enquiry("YUKI.N > geometry")
        return self._get_scaled(pixbuf, yuki_geometry)

    def __init__(self, parent):
        self._parent = parent
