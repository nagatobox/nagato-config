
from libnagatoconfig.ui.box.Box import NagatoBox
from libnagatoconfig.ui.box.user.Entry import NagatoEntry


class NagatoDirectory(NagatoBox):

    KEY = "DEFINE DIRECTORY HERE"

    def _inform_key(self):
        return self.KEY

    def _initialize_control(self):
        self.set_homogeneous(True)
        self.pack_start(NagatoEntry(self), True, True, 0)
