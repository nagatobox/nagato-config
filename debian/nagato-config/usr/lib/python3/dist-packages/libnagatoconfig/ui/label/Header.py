
from libnagato4.Ux import Unit
from libnagatoconfig.ui.label.Label import NagatoLabel


class NagatoHeader(NagatoLabel):

    def _setup(self):
        self.set_size_request(-1, Unit(6))
        self.set_text(self._parent.HEADER_LABEL)
        self._raise("YUKI.N > css", (self, "config-box-odd"))
