
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.widget import Margin


class NagatoComboBox(Gtk.ComboBox, NagatoObject):

    def _get_list_store(self):
        return self._enquiry("YUKI.N > list store", self._parent.LIST_KEY)

    def _get_current_index(self):
        yuki_current = self._enquiry("YUKI.N > current", self._parent.GTK_KEY)
        for yuki_tree_model_row in self.get_model():
            if yuki_tree_model_row[0] == yuki_current:
                return yuki_tree_model_row.path.get_indices()[0]
        return 0

    def _on_changed(self, combo_box):
        yuki_model = self.get_model()
        yuki_current = yuki_model.get_value(self.get_active_iter(), 0)
        yuki_data = self._parent.GTK_KEY, yuki_current
        self._raise("YUKI.N > change current", yuki_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.ComboBox.__init__(self)
        self.set_model(self._get_list_store())
        yuki_cell = Gtk.CellRendererText()
        self.pack_start(yuki_cell, True)
        self.add_attribute(yuki_cell, "text", 0)
        self.set_active(self._get_current_index())
        self.set_wrap_width(int(len(self.get_model())/15))
        Margin.set_with_unit(self, "margin", "margin", "margin", "margin")
        self.connect("changed", self._on_changed)
