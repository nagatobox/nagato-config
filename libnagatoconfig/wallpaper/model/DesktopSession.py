
from gi.repository import GLib


def get():
    for yuki_splice in GLib.get_environ():
        if yuki_splice.startswith("DESKTOP_SESSION="):
            return yuki_splice.replace("DESKTOP_SESSION=", "")
    return "UNKNOWN"
