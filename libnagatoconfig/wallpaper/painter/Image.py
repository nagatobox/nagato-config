
from libnagato4.Object import NagatoObject
from libnagatoconfig.wallpaper.painter.Pixbuf import NagatoPixbuf


class NagatoImage(NagatoObject):

    def paint(self, cairo_context):
        self._pixbuf.refresh(cairo_context)

    def __init__(self, parent):
        self._parent = parent
        self._pixbuf = NagatoPixbuf(self)
