
from gi.repository import GdkPixbuf
from libnagato4.Object import NagatoObject
from libnagatoconfig.wallpaper.scaler.Scalers import NagatoScalers
from libnagatoconfig.wallpaper.painter.PixbufPattern import NagatoPixbufPattern

TEST_PATH_X = "/home/takedanemuru/avatar.png"
TEST_PATH = "/home/takedanemuru/main-window.png"


class NagatoPixbuf(NagatoObject):

    def _inform_scaled_pixbuf(self):
        return self._pixbuf

    def refresh(self, cairo_context):
        yuki_pixbuf = GdkPixbuf.Pixbuf.new_from_file(TEST_PATH)
        self._pixbuf = self._scalers.get_scaled(yuki_pixbuf)
        self._pattern.paint(cairo_context)

    def __init__(self, parent):
        self._parent = parent
        self._scalers = NagatoScalers(self)
        self._pattern = NagatoPixbufPattern(self)
