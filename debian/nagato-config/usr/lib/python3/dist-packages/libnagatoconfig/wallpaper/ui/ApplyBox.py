
from libnagatoconfig.ui.box.Box import NagatoBox
from libnagatoconfig.ui.label.Spacer import NagatoSpacer
from libnagatoconfig.ui.button.GoBack import NagatoGoBack
from libnagatoconfig.ui.button.Apply import NagatoApply as NagatoApplyButton


class NagatoApplyBox(NagatoBox):

    def _initialize_label(self):
        self.set_center_widget(NagatoSpacer(self))

    def _initialize_control(self):
        self._apply_button = NagatoApplyButton(self)
        self.pack_start(NagatoGoBack(self), False, False, 0)
        self.pack_end(self._apply_button, False, False, 0)
        self._raise("YUKI.N > add to box", self)

    def set_sensitive(self, applicable):
        self._apply_button.set_sensitive(applicable)
