
from libnagatoconfig.ui.box.appearance.Appearance import NagatoAppearance
from libnagatoconfig.ui.box.user.ComboBox import NagatoComboBox


class NagatoWidgets(NagatoAppearance):

    LABEL_TEXT = "Widgets"
    CSS = "config-box-odd"
    LIST_KEY = "themes"
    GTK_KEY = "gtk-theme-name"

    def _initialize_control_widget(self):
        self.pack_start(NagatoComboBox(self), True, True, 0)
