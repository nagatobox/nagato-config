
from libnagato4.Ux import Unit
from libnagato4.Object import NagatoObject

TILE_SIZE = Unit(4)


class NagatoCheckerBoard(NagatoObject):

    def _get_range(self, board_size, tile_size):
        yuki_start = -1*(tile_size-(board_size%tile_size)/2)
        return int(yuki_start), board_size, int(tile_size)

    def _paint_tile(self, cairo_context, x, y):
        yuki_is_black = ((x+y)//TILE_SIZE%2 == 0)
        yuki_rgba = (0, 0, 0, 0.2) if yuki_is_black else (1, 1, 1, 0.2)
        cairo_context.set_source_rgba(*yuki_rgba)
        cairo_context.rectangle(x, y, TILE_SIZE, TILE_SIZE)
        cairo_context.fill()

    def paint(self, cairo_context):
        yuki_size = self._enquiry("YUKI.N > allocated size")
        yuki_x_range = self._get_range(yuki_size.width, TILE_SIZE)
        yuki_y_range = self._get_range(yuki_size.height, TILE_SIZE)
        for yuki_x in range(*yuki_x_range):
            for yuki_y in range(*yuki_y_range):
                self._paint_tile(cairo_context, yuki_x, yuki_y)

    def __init__(self, parent):
        self._parent = parent
