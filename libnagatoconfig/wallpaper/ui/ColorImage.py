
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatoconfig.wallpaper.ui import Surface


class NagatoColorImage(Gtk.Image, NagatoObject):

    def _refresh(self):
        yuki_rgba = self._enquiry("YUKI.N > wallpaper config", "rgba")
        yuki_image_surface = Surface.new_from_rgba(yuki_rgba)
        self.set_from_surface(yuki_image_surface)

    def receive_transmission(self, user_data):
        self._refresh()

    def __init__(self, parent):
        self._parent = parent
        Gtk.Image.__init__(self)
        self._refresh()
        self._raise("YUKI.N > register model object", self)
