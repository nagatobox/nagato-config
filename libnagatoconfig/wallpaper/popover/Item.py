
from libnagato4.popover.item.Item import NagatoItem as TFEI


class NagatoItem(TFEI):

    MESSAGE = "YUKI.N > wallpaper config"

    @classmethod
    def new_for_data(cls, parent, label, user_data):
        yuki_item = NagatoItem(parent)
        yuki_item.set_label(label)
        yuki_item.USER_DATA = "pattern", user_data
