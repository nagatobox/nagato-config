
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.widget import Margin
from libnagato4.Ux import Unit
from libnagato4.chooser.context.Color import NagatoContext
from libnagato4.chooser.color.Dialog import NagatoDialog
from libnagatoconfig.wallpaper.ui.ColorImage import NagatoColorImage


class NagatoColorButton(Gtk.Button, NagatoObject):

    def _on_clicked(self, button):
        yuki_rgba = self._enquiry("YUKI.N > wallpaper config", "rgba")
        yuki_context = NagatoContext.new(self, "select color", yuki_rgba)
        yuki_response = NagatoDialog.run_with_context(self, yuki_context)
        if yuki_response == Gtk.ResponseType.APPLY:
            yuki_data = "rgba", yuki_context["selection"]
            self._raise("YUKI.N > wallpaper config", yuki_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(self, relief=Gtk.ReliefStyle.NONE)
        self.set_image(NagatoColorImage(self))
        Margin.set_with_unit(self, "margin", "margin", "margin", "margin")
        self.set_size_request(Unit("button-width"), -1)
        self.connect("clicked", self._on_clicked)
        self._raise("YUKI.N > add to box", self)
