
from libnagatoconfig.ui.box.directory.Directory import NagatoDirectory


class NagatoDocuments(NagatoDirectory):

    KEY = "DOCUMENTS"
    LABEL_TEXT = "DOCUMENTS"
    CSS = "config-box-even"
