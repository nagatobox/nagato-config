
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.Ux import Unit
from libnagatoconfig.ui.label.Description import NagatoDescrption


class NagatoBox(Gtk.Box, NagatoObject):

    LABEL_TEXT = "Dont't look at me."
    CSS = "config-box-odd"

    def _initialize_label(self):
        self.pack_start(NagatoDescrption(self), True, True, 0)

    def _initialize_control(self):
        pass

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        self.set_size_request(-1, Unit(6))
        self._raise("YUKI.N > css", (self, self.CSS))
        self._initialize_label()
        self._initialize_control()
