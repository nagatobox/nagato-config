
from libnagato4.Object import NagatoObject
from libnagato4.Transmitter import HaruhiTransmitter
from libnagatoconfig.wallpaper.Model import NagatoModel
from libnagatoconfig.wallpaper.ui.Widgets import AsakuraWidgets


class NagatoModelLayer(NagatoObject):

    def _yuki_n_register_model_object(self, object_):
        self._transmitter.register_listener(object_)

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = HaruhiTransmitter()
        self._model = NagatoModel(self)
        AsakuraWidgets(self)
