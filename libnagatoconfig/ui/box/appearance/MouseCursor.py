
from libnagatoconfig.ui.box.appearance.Appearance import NagatoAppearance
from libnagatoconfig.ui.box.user.ComboBox import NagatoComboBox


class NagatoMouseCursor(NagatoAppearance):

    LABEL_TEXT = "Mouse Cursor"
    CSS = "config-box-even"
    LIST_KEY = "cursors"
    GTK_KEY = "gtk-cursor-theme-name"

    def _initialize_control_widget(self):
        self.pack_start(NagatoComboBox(self), True, True, 0)
