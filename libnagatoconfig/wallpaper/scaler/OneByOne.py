
from libnagatoconfig.wallpaper.scaler.Scaler import AbstractScaler


class NagatoOneByOne(AbstractScaler):

    def get_non_scaled(self, pixbuf):
        yuki_rate = self._enquiry("YUKI.N > rate")
        return self._build(pixbuf, yuki_rate)
