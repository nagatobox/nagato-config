
from libnagato4.Object import NagatoObject
from libnagatoconfig.data.parser.Themes import NagatoThemes
from libnagatoconfig.data.parser.Icons import NagatoIcons


class NagatoData(NagatoObject):

    def __getitem__(self, key):
        return sorted(self._data[key])

    def _yuki_n_append_data(self, user_data):
        yuki_key, yuki_value = user_data
        self._data[yuki_key].append(yuki_value)

    def __init__(self, parent):
        self._parent = parent
        self._data = {"themes": [], "icons": [], "cursors": []}
        NagatoThemes(self)
        NagatoIcons(self)
