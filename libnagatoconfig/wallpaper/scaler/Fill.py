
from gi.repository import GdkPixbuf
from libnagatoconfig.wallpaper.scaler.Scaler import AbstractScaler

INTERP = GdkPixbuf.InterpType.BILINEAR


class NagatoFill(AbstractScaler):

    def _get_scaled(self, pixbuf, geometry):
        _, _, yuki_width, yuki_height = geometry
        return pixbuf.scale_simple(yuki_width, yuki_height, INTERP)
