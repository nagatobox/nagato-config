
from libnagatoconfig.ui.box.Box import NagatoBox


class NagatoAppearance(NagatoBox):

    LABEL_TEXT = "DEFINE TEXT LABEL HERE, DO NOT LOOK AT ME."
    CSS = "config-box-odd"

    def _initialize_control_widget(self):
        pass

    def _initialize_control(self):
        self.set_homogeneous(True)
        self._initialize_control_widget()
