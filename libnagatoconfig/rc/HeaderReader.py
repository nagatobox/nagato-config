
from libnagato4.Object import NagatoObject


class NagatoHeaderReader(NagatoObject):

    def parse(self, line):
        if line.startswith("[") and line.endswith("]"):
            yuki_header_name = line[1:-1]
            self._raise("YUKI.N > new header", yuki_header_name)

    def __init__(self, parent):
        self._parent = parent
