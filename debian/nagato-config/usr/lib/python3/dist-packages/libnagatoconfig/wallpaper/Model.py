
from libnagato4.Object import NagatoObject
from libnagatoconfig.wallpaper.Pixbuf import NagatoPixbuf
from libnagatoconfig.wallpaper.model.Background import NagatoBackground


class NagatoModel(NagatoObject):

    def __init__(self, parent):
        self._parent = parent
        NagatoPixbuf(self)
