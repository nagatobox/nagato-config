
from libnagatoconfig.rc.data.KeyValue import NagatoKeyValue


class NagatoGroup(object):

    def __setitem__(self, key, value):
        if key in self._data:
            self._data[key].set_value(value)

    def __getitem__(self, key):
        if key in self._data:
            return self._data[key].value
        return None

    def __len__(self):
        return len(self._data)

    def add_key_value(self, user_data):
        yuki_key, _, _ = user_data
        yuki_key_value = NagatoKeyValue(user_data)
        self._data[yuki_key] = yuki_key_value

    def get_text(self):
        yuki_text = ""
        for yuki_key, yuki_value in self._data.items():
            yuki_text += "{}={}\n".format(yuki_key, yuki_value.get_text())
        return yuki_text

    def __init__(self, name):
        self._name = name
        self._data = {}
