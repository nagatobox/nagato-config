
from libnagato4.Object import NagatoObject


class NagatoPositions(NagatoObject):

    def _get_x(self, width, pattern, geometry):
        yuki_x, _, yuki_width, _ = geometry
        if pattern.endswith("left"):
            return yuki_x
        if pattern.endswith("right"):
            return (yuki_x+yuki_width)-width
        return yuki_x+(yuki_width-width)/2

    def _get_y(self, height, pattern, geometry):
        _, yuki_y, _, yuki_height = geometry
        if pattern.startswith("top"):
            return yuki_y
        if pattern.startswith("bottom"):
            return (yuki_y+yuki_height)-height
        return yuki_y+(yuki_height-height)/2

    def get(self, pixbuf, pattern):
        yuki_geometry = self._enquiry("YUKI.N > geometry")
        yuki_x = self._get_x(pixbuf.get_width(), pattern, yuki_geometry)
        yuki_y = self._get_y(pixbuf.get_height(), pattern, yuki_geometry)
        return yuki_x, yuki_y

    def __init__(self, parent):
        self._parent = parent
