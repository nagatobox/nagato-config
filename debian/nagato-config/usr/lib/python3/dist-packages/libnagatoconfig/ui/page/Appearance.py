
from libnagatoconfig.ui.page.Page import NagatoPage
from libnagatoconfig.model.ForAppearance import NagatoModel
from libnagatoconfig.ui.box.appearance.Widgets import NagatoWidgets
from libnagatoconfig.ui.box.appearance.Icons import NagatoIcons
from libnagatoconfig.ui.box.appearance.Font import NagatoFont
from libnagatoconfig.ui.box.appearance.MouseCursor import NagatoMouseCursor
from libnagatoconfig.ui.label.Spacer import NagatoSpacer
from libnagatoconfig.ui.box.Apply import NagatoApply


class NagatoAppearance(NagatoPage):

    HEADER_LABEL = "Appearance"

    def _yuki_n_change_current(self, user_data):
        self._apply.set_sensitive(True)
        self._model.set_current_setting(user_data)

    def _yuki_n_apply_changes(self):
        self._model.save()

    def _inform_current(self, key):
        return self._model.get_current_setting(key)

    def _inform_list_store(self, key):
        return self._model.get_list_store(key)

    def _initialize_children(self):
        self._model = NagatoModel(self)
        self.pack_start(NagatoWidgets(self), False, False, 0)
        self.pack_start(NagatoIcons(self), False, False, 0)
        self.pack_start(NagatoFont(self), False, False, 0)
        self.pack_start(NagatoMouseCursor(self), False, False, 0)
        self.pack_start(NagatoSpacer(self), True, False, 0)
        self._apply = NagatoApply(self)
        self.pack_start(self._apply, False, False, 0)
