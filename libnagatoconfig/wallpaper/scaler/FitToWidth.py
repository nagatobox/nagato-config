
from libnagatoconfig.wallpaper.scaler.Scaler import AbstractScaler


class NagatoFitToWidth(AbstractScaler):

    def _get_scaled(self, pixbuf, geometry):
        _, _, yuki_base_width, _ = geometry
        yuki_rate = yuki_base_width/pixbuf.get_width()
        return self._build(pixbuf, yuki_rate)
