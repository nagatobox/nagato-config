
from libnagatoconfig.wallpaper.scaler.Scaler import AbstractScaler


class NagatoFitToHeight(AbstractScaler):

    def _get_scaled(self, pixbuf, geometry):
        _, _, _, yuki_base_height = geometry
        yuki_rate = yuki_base_height/pixbuf.get_height()
        return self._build(pixbuf, yuki_rate)
