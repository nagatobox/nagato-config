
from libnagatoconfig.ui.box.directory.Desktop import NagatoDesktop
from libnagatoconfig.ui.box.directory.Documents import NagatoDocuments
from libnagatoconfig.ui.box.directory.Download import NagatoDownload
from libnagatoconfig.ui.box.directory.Music import NagatoMusic
from libnagatoconfig.ui.box.directory.Pictures import NagatoPictures
from libnagatoconfig.ui.box.directory.PublicShare import NagatoPublicShare
from libnagatoconfig.ui.box.directory.Templates import NagatoTemplates
from libnagatoconfig.ui.box.directory.Videos import NagatoVideos


class AsakuraXdgUserDirs(object):

    def __init__(self, parent):
        parent.pack_start(NagatoDesktop(parent), False, False, 0)
        parent.pack_start(NagatoDocuments(parent), False, False, 0)
        parent.pack_start(NagatoDownload(parent), False, False, 0)
        parent.pack_start(NagatoMusic(parent), False, False, 0)
        parent.pack_start(NagatoPictures(parent), False, False, 0)
        parent.pack_start(NagatoPublicShare(parent), False, False, 0)
        parent.pack_start(NagatoTemplates(parent), False, False, 0)
        parent.pack_start(NagatoVideos(parent), False, False, 0)
