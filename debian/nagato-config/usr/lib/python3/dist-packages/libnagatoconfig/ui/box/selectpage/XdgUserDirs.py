
from libnagatoconfig.ui.box.selectpage.SelectPage import NagatoSelectPage


class NagatoXdgUserDirs(NagatoSelectPage):

    LABEL_TEXT = "XDG User Directories"
    BUTTON_MESSAGE = "xdg-user-dirs"
