
from gi.repository import Gtk
from libnagato4.Object import NagatoObject


class NagatoLabel(Gtk.Label, NagatoObject):

    def _setup(self):
        pass

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(self)
        self._setup()
