
from gi.repository import Gdk
from libnagato4.Object import NagatoObject


class NagatoBackground(NagatoObject):

    def paint(self, cairo_context):
        yuki_pattern = self._enquiry("YUKI.N > wallpaper config", "pattern")
        if yuki_pattern in ("fill", "tile"):
            return
        yuki_rgba = self._enquiry("YUKI.N > wallpaper config", "rgba")
        Gdk.cairo_set_source_rgba(cairo_context, yuki_rgba)
        yuki_geometry = self._enquiry("YUKI.N > geometry")
        cairo_context.rectangle(*yuki_geometry)
        cairo_context.fill()

    def __init__(self, parent):
        self._parent = parent
