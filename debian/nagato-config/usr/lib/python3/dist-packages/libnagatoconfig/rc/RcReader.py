
from pathlib import Path
from libnagato4.Object import NagatoObject
from libnagatoconfig.rc.HeaderReader import NagatoHeaderReader
from libnagatoconfig.rc.KeyValueReader import NagatoKeyValueReader


class NagatoRcReader(NagatoObject):

    def _parse_a_line(self, lines):
        for yuki_line in lines:
            self._header_reader.parse(yuki_line.rstrip())
            self._key_value_reader.parse(yuki_line.rstrip())

    def _parse(self):
        with Path(self._parent.PATH).open() as yuki_file:
            self._parse_a_line(yuki_file.readlines())

    def __init__(self, parent):
        self._parent = parent
        self._header_reader = NagatoHeaderReader(self)
        self._key_value_reader = NagatoKeyValueReader(self)
        self._parse()
