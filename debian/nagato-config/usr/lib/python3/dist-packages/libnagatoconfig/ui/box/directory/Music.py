
from libnagatoconfig.ui.box.directory.Directory import NagatoDirectory


class NagatoMusic(NagatoDirectory):

    KEY = "MUSIC"
    LABEL_TEXT = "MUSIC"
    CSS = "config-box-even"
