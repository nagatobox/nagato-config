
from libnagatoconfig.ui.box.directory.Directory import NagatoDirectory


class NagatoPublicShare(NagatoDirectory):

    KEY = "PUBLICSHARE"
    LABEL_TEXT = "PUBLICSHARE"
    CSS = "config-box-even"
