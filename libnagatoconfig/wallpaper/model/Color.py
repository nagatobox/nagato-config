
from gi.repository import Gdk
from libnagato4.Object import NagatoObject
from libnagatoconfig.wallpaper.model import DesktopSession


class NagatoColor(Gdk.RGBA, NagatoObject):

    def set_data(self, data):
        self.parse(data.to_string())

    def get_data(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        yuki_query = DesktopSession.get(), "color", "White"
        yuki_current_setting = self._enquiry("YUKI.N > settings", yuki_query)
        Gdk.RGBA.__init__(self)
        self.parse(yuki_current_setting)
