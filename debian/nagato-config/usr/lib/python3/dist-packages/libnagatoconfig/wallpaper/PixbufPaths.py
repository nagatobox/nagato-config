
from gi.repository import Gio
from gi.repository import GLib
from libnagato4.Object import NagatoObject


class NagatoPixbufPaths(NagatoObject):

    def _ensure_directory(self):
        yuki_gio_file = Gio.File.new_for_path(self._directory)
        if not yuki_gio_file.query_exists():
            yuki_gio_file.make_directory_with_parents()

    def _initialize_directory(self):
        yuki_id = self._enquiry("YUKI.N > data", "id")
        for yuki_splice in GLib.get_environ():
            if yuki_splice.startswith("DESKTOP_SESSION="):
                yuki_session = yuki_splice.replace("DESKTOP_SESSION=", "")
        yuki_names = [GLib.get_user_config_dir(), yuki_id, yuki_session]
        self._directory = GLib.build_filenamev(yuki_names)

    def get_path(self, basename):
        yuki_path = GLib.build_filenamev([self._directory, basename])
        yuki_exists = GLib.file_test(yuki_path, GLib.FileTest.EXISTS)
        return yuki_path if yuki_exists else None

    def __init__(self, parent):
        self._parent = parent
        self._initialize_directory()
        self._ensure_directory()
