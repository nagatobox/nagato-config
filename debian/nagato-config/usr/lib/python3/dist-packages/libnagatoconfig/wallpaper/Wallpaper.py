
from libnagatoconfig.ui.page.Page import NagatoPage
from libnagatoconfig.wallpaper.model.ModelLayer import NagatoModelLayer


class NagatoWallpaper(NagatoPage):

    HEADER_LABEL = "Wallpaper"
    ID = "wallpaper"

    def _yuki_n_add_to_box(self, widget):
        self.add(widget)

    def _initialize_children(self):
        NagatoModelLayer(self)
        self._raise("YUKI.N > add to stack named", (self, self.ID))

    """
    def _inform_directory(self, key):
        return self._model[key]

    def _yuki_n_change_directory(self, user_data):
        yuki_key, yuki_value = user_data
        self._model[yuki_key] = yuki_value

    def _yuki_n_change_applicable(self, applicable):
        self._apply_box.set_sensitive(applicable)

    def _yuki_n_apply_changes(self):
        self._model.apply_changes()
    """
