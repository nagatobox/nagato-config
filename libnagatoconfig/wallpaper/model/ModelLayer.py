
from libnagato4.Object import NagatoObject
from libnagato4.Transmitter import HaruhiTransmitter
from libnagatoconfig.wallpaper.model.Model import NagatoModel
from libnagatoconfig.wallpaper.ui.Widgets import AsakuraWidgets


class NagatoModelLayer(NagatoObject):

    def _inform_wallpaper_config(self, key):
        return self._model[key]

    def _yuki_n_wallpaper_config(self, user_data):
        yuki_key, yuki_value = user_data
        self._model[yuki_key] = yuki_value
        self._transmitter.transmit(user_data)

    def _yuki_n_register_model_object(self, object_):
        self._transmitter.register_listener(object_)

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = HaruhiTransmitter()
        self._model = NagatoModel(self)
        AsakuraWidgets(self)
