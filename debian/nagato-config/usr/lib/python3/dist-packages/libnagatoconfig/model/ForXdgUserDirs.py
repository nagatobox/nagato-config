
from libnagato4.Object import NagatoObject
from libnagatoconfig.Mikuru import XdgUserDirs


class NagatoModel(NagatoObject):

    def _get_applicable(self):
        for yuki_key, _ in XdgUserDirs.GLIB_INDEX.items():
            if self._original_data[yuki_key] != self._current_data[yuki_key]:
                return True
        return False

    def apply_changes(self):
        for yuki_key, _ in XdgUserDirs.GLIB_INDEX.items():
            if self._original_data[yuki_key] != self._current_data[yuki_key]:
                XdgUserDirs.update(yuki_key, self._current_data[yuki_key])
        self._raise("YUKI.N > header notify", ("info", "It's done."))

    def __getitem__(self, key):
        return self._current_data[key]

    def __setitem__(self, key, value):
        self._current_data[key] = value
        self._raise("YUKI.N > change applicable", self._get_applicable())

    def __init__(self, parent):
        self._parent = parent
        self._original_data, self._current_data = XdgUserDirs.get_data_pair()
