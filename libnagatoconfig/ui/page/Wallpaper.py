
from libnagatoconfig.ui.page.Page import NagatoPage
from libnagatoconfig.ui.label.Spacer import NagatoSpacer
from libnagatoconfig.ui.box.Apply import NagatoApply


class NagatoWallpaper(NagatoPage):

    HEADER_LABEL = "Wallpaper"

    def _initialize_children(self):
        self._apply_box = NagatoApply(self)
        self.add(NagatoSpacer(self))
        self.add(self._apply_box)
