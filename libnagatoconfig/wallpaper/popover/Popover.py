
from libnagato4.popover.SingleStack import AbstractSingleStack
from libnagato4.popover.Separator import NagatoSeparator
from libnagatoconfig.wallpaper.popover.Item import NagatoItem


class NagatoPopover(AbstractSingleStack):

    def _add_to_single_stack(self):
        NagatoItem.new_for_data(self, "center", "center")
        NagatoItem.new_for_data(self, "tile", "tile")
        NagatoSeparator(self)
        NagatoItem.new_for_data(self, "fill", "fill")
        NagatoItem.new_for_data(self, "fit-to-width", "fit-to-width")
        NagatoItem.new_for_data(self, "fit-to-height", "fit-to-height")
        NagatoSeparator(self)
        NagatoItem.new_for_data(self, "top-left", "top-left")
        NagatoItem.new_for_data(self, "top", "top")
        NagatoItem.new_for_data(self, "top-right", "top-right")
        NagatoItem.new_for_data(self, "left", "left")
        NagatoItem.new_for_data(self, "center", "center")
        NagatoItem.new_for_data(self, "right", "right")
        NagatoItem.new_for_data(self, "bottom-left", "bottom-left")
        NagatoItem.new_for_data(self, "bottom", "bottom")
        NagatoItem.new_for_data(self, "bottom-right", "bottom-right")
