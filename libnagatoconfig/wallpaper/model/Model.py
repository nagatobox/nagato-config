
from libnagato4.Object import NagatoObject
from libnagatoconfig.wallpaper.model.Pixbuf import NagatoPixbuf
from libnagatoconfig.wallpaper.model.Color import NagatoColor
from libnagatoconfig.wallpaper.model.Pattern import NagatoPattern

MODEL_ITEMS = {"rgba": "_rgba", "pixbuf": "_pixbuf", "pattern": "_pattern"}


class NagatoModel(NagatoObject):

    def __setitem__(self, key, value):
        yuki_item = getattr(self, MODEL_ITEMS[key])
        yuki_item.set_data(value)

    def __getitem__(self, key):
        yuki_item = getattr(self, MODEL_ITEMS[key])
        return yuki_item.get_data()

    def apply(self):
        pass

    def __init__(self, parent):
        self._parent = parent
        self._pixbuf = NagatoPixbuf(self)
        self._rgba = NagatoColor(self)
        self._pattern = NagatoPattern(self)
