
from libnagatoconfig.ui.button.Button import NagatoButton


class NagatoApply(NagatoButton):

    LABEL_TEXT = "Apply"
    CSS = "button-error"
    DEFAULT_SENSITIVE = False

    def _on_clicked(self, button):
        self._raise("YUKI.N > apply changes")
