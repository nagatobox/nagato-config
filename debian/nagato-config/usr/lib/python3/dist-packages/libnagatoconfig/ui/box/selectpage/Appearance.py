
from libnagatoconfig.ui.box.selectpage.SelectPage import NagatoSelectPage


class NagatoAppearance(NagatoSelectPage):

    LABEL_TEXT = "Appearance"
    BUTTON_MESSAGE = "appearance"
    CSS = "config-box-even"
