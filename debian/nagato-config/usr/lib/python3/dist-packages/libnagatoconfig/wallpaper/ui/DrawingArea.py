
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatoconfig.wallpaper.painter.Painters import AsakuraPainters


class NagatoDrawingArea(Gtk.DrawingArea, NagatoObject):

    def _inform_allocated_size(self):
        yuki_rectangle, _ = self.get_allocated_size()
        return yuki_rectangle

    def _on_draw(self, drawing_area, cairo_context):
        self._painters.paint(cairo_context)

    def __init__(self, parent):
        self._parent = parent
        Gtk.DrawingArea.__init__(self)
        self.set_hexpand(True)
        self.set_vexpand(True)
        self._painters = AsakuraPainters(self)
        self.connect("draw", self._on_draw)
        self._raise("YUKI.N > add to box", self)
