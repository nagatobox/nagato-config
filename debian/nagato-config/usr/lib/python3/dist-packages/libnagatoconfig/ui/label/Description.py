
from libnagato4.Ux import Unit
from libnagatoconfig.ui.label.Label import NagatoLabel


class NagatoDescrption(NagatoLabel):

    def _setup(self):
        self.set_text(self._parent.LABEL_TEXT)
        self.set_xalign(0)
        self.set_margin_start(Unit(2))
