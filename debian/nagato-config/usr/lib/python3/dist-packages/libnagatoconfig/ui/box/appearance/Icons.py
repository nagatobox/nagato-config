
from libnagatoconfig.ui.box.appearance.Appearance import NagatoAppearance
from libnagatoconfig.ui.box.user.ComboBox import NagatoComboBox


class NagatoIcons(NagatoAppearance):

    LABEL_TEXT = "Icons"
    CSS = "config-box-even"
    LIST_KEY = "icons"
    GTK_KEY = "gtk-icon-theme-name"

    def _initialize_control_widget(self):
        self.pack_start(NagatoComboBox(self), True, True, 0)
