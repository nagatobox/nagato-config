
from libnagatoconfig.data.rc.Gtk2 import NagatoGtk2
from libnagatoconfig.data.rc.Gtk3 import NagatoGtk3
from libnagato4.Object import NagatoObject


class NagatoPrimaryDataPair(NagatoObject):

    def set_current_setting(self, user_data):
        yuki_key, yuki_value = user_data
        self._gtk2[""][yuki_key] = yuki_value
        self._gtk3["Settings"][yuki_key] = yuki_value

    def __getitem__(self, key):
        return self._gtk3["Settings"][key]

    def save(self):
        self._gtk2.save()
        self._gtk3.save()

    def __init__(self, parent):
        self._parent = parent
        self._gtk2 = NagatoGtk2(self)
        self._gtk3 = NagatoGtk3(self)
