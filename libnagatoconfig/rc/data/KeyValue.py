
FORMATS = {
    0: "{}",
    1: "'{}'",
    2: '"{}"'
}


class NagatoKeyValue(object):

    def _get_value(self, value):
        if self._quoted == 0:
            return value
        return value[1:-1]

    def set_value(self, value):
        self._value = value

    def get_text(self):
        yuki_format = FORMATS[self._quoted]
        return yuki_format.format(self._value)

    def __init__(self, user_data):
        self._key, yuki_value, self._quoted = user_data
        self._value = self._get_value(yuki_value)

    @property
    def key(self):
        return self._key

    @property
    def value(self):
        return self._value
