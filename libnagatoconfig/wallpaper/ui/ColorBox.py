
from libnagatoconfig.ui.box.Box import NagatoBox
from libnagatoconfig.wallpaper.ui.ColorButton import NagatoColorButton


class NagatoColorBox(NagatoBox):

    LABEL_TEXT = "Background Color"
    CSS = "config-box-odd"

    def _yuki_n_add_to_box(self, widget):
        self.add(widget)

    def _initialize_control(self):
        NagatoColorButton(self)
        self._raise("YUKI.N > add to box", self)
