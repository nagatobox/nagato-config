
from libnagatoconfig.ui.button.Button import NagatoButton


class NagatoGo(NagatoButton):

    LABEL_TEXT = "Go"

    def _on_clicked(self, button):
        self._raise("YUKI.N > page move to", self._parent.BUTTON_MESSAGE)
