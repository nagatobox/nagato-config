
from gi.repository import Gio

FLAG = Gio.FileQueryInfoFlags.NONE


def get_all_names(directory):
    yuki_names = []
    for yuki_file_info in yield_children(directory):
        yuki_names.append(yuki_file_info.get_name())
    return yuki_names


def yield_children(directory):
    yuki_file = Gio.File.new_for_path(directory)
    for yuki_file_info in yuki_file.enumerate_children("*", FLAG, None):
        yield yuki_file_info
