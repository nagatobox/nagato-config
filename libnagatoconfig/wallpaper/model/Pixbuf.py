
from gi.repository import GdkPixbuf
from libnagato4.Object import NagatoObject
from libnagatoconfig.wallpaper.model.PixbufPaths import NagatoPixbufPaths


class NagatoPixbuf(NagatoObject):

    def set_data(self, path):
        self._source_pixbuf = GdkPixbuf.Pixbuf.new_from_file(path)

    def get_data(self):
        return self._source_pixbuf

    def __init__(self, parent):
        self._parent = parent
        self._source_pixbuf = None
        self._paths = NagatoPixbufPaths(self)
        yuki_path = self._paths.get_path("source.png")
        if yuki_path is not None:
            self._source_pixbuf = GdkPixbuf.Pixbuf.new_from_file(yuki_path)
