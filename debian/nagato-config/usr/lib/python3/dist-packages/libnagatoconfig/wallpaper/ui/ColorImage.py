
import cairo
from gi.repository import Gtk
from gi.repository import Gdk
from libnagato.Ux import Unit
from libnagato4.Object import NagatoObject

FORMAT = cairo.Format.RGB24
WIDTH = Unit(7)
HEIGHT = Unit(4)


class NagatoColorImage(Gtk.Image, NagatoObject):

    def _get_current_rgba(self):
        yuki_rgba = Gdk.RGBA()
        yuki_rgba.parse("White")
        return yuki_rgba

    def _refresh(self):
        yuki_image_surface = cairo.ImageSurface(FORMAT, WIDTH, HEIGHT)
        yuki_cairo_context = cairo.Context(yuki_image_surface)
        yuki_rgba = self._get_current_rgba()
        Gdk.cairo_set_source_rgba(yuki_cairo_context, yuki_rgba)
        yuki_cairo_context.rectangle(0, 0, WIDTH, HEIGHT)
        yuki_cairo_context.fill()
        self.set_from_surface(yuki_image_surface)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Image.__init__(self)
        self._refresh()
