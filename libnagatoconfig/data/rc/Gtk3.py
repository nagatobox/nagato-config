
from pathlib import Path
from libnagatoconfig.data.rc.Rc import NagatoRc


class NagatoGtk3(NagatoRc):

    PATH = Path.home().joinpath(".config/gtk-3.0/settings.ini")
