
from libnagato4.Object import NagatoObject

QUOTED_NONE = 0
QUOTED_SINGLE = 1
QUOTED_DOUBLE = 2


class NagatoKeyValueReader(NagatoObject):

    def _starts_ends(self, value, target):
        if value.startswith(target) and value.endswith(target):
            return True
        return False

    def _get_quoted_type(self, value):
        if self._starts_ends(value, "'"):
            return QUOTED_SINGLE
        if self._starts_ends(value, '"'):
            return QUOTED_DOUBLE
        return QUOTED_NONE

    def parse(self, line):
        yuki_data = line.split("=")
        if len(yuki_data) == 2:
            yuki_key, yuki_value = yuki_data
            yuki_quoted = self._get_quoted_type(yuki_value)
            yuki_data = yuki_key, yuki_value, yuki_quoted
            self._raise("YUKI.N > new key value", yuki_data)

    def __init__(self, parent):
        self._parent = parent
