
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.Ux import Unit
from libnagato4.widget import Margin


class NagatoButton(Gtk.Button, NagatoObject):

    LABEL_TEXT = "Don't look at me."
    CSS = "button-safe"
    DEFAULT_SENSITIVE = True

    def _on_clicked(self, button):
        pass

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(self)
        self.set_label(self.LABEL_TEXT)
        self.set_sensitive(self.DEFAULT_SENSITIVE)
        self.set_relief(Gtk.ReliefStyle.NONE)
        Margin.set_with_unit(self, "margin", "margin", "margin", "margin")
        self.set_size_request(Unit("button-width"), -1)
        self._raise("YUKI.N > css", (self, self.CSS))
        self.connect("clicked", self._on_clicked)
