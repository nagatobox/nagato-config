
from gi.repository import Gdk
from libnagato4.Object import NagatoObject
from libnagatoconfig.wallpaper.pixbuf.Positions import NagatoPositions
from libnagatoconfig.wallpaper.pixbuf.TilePositions import HaruhiTilePositions


class NagatoPixbufPattern(NagatoObject):

    def _paint_once(self, cairo_context, pixbuf, pattern):
        yuki_x, yuki_y = self._positions.get(pixbuf, pattern)
        Gdk.cairo_set_source_pixbuf(cairo_context, pixbuf, yuki_x, yuki_y)
        cairo_context.paint()

    def _paint_tile(self, cairo_context, pixbuf):
        yuki_geometry = self._enquiry("YUKI.N > geometry")
        cairo_context.rectangle(*yuki_geometry)
        cairo_context.clip()
        for yuki_x, yuki_y in self._tile_positions.get(yuki_geometry, pixbuf):
            Gdk.cairo_set_source_pixbuf(cairo_context, pixbuf, yuki_x, yuki_y)
            cairo_context.paint()

    def paint(self, cairo_context):
        yuki_pixbuf = self._enquiry("YUKI.N > scaled pixbuf")
        yuki_pattern = self._enquiry("YUKI.N > wallpaper config", "pattern")
        if yuki_pattern != "tile":
            self._paint_once(cairo_context, yuki_pixbuf, yuki_pattern)
        else:
            self._paint_tile(cairo_context, yuki_pixbuf)

    def __init__(self, parent):
        self._parent = parent
        self._positions = NagatoPositions(self)
        self._tile_positions = HaruhiTilePositions()
