
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatoconfig.ui.label.Header import NagatoHeader
from libnagatoconfig.ui.label.Spacer import NagatoSpacer


class NagatoPage(NagatoObject, Gtk.Box):

    HEADER_LABEL = "Don't look at me."

    def _initialize_header(self):
        self.add(NagatoHeader(self))
        self.add(NagatoSpacer(self))

    def _initialize_children(self):
        pass

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        self.set_hexpand(True)
        self.set_vexpand(True)
        self._initialize_header()
        self._initialize_children()
        self.show()
