
from libnagato4.Object import NagatoObject
from libnagatoconfig.wallpaper.scaler.OneByOne import NagatoOneByOne
from libnagatoconfig.wallpaper.scaler.Fill import NagatoFill
from libnagatoconfig.wallpaper.scaler.FitToWidth import NagatoFitToWidth
from libnagatoconfig.wallpaper.scaler.FitToHeight import NagatoFitToHeight


class NagatoPixbufScaler(NagatoObject):

    def get_scaled(self, pixbuf):
        yuki_pattern = self._enquiry("YUKI.N > wallpaper config", "pattern")
        if yuki_pattern in self._specials:
            yuki_scaler = self._specials[yuki_pattern]
            return yuki_scaler.get_scaled(pixbuf)
        else:
            return self._one_by_one.get_non_scaled(pixbuf)

    def __init__(self, parent):
        self._parent = parent
        self._one_by_one = NagatoOneByOne(self)
        self._specials = {
            "fill": NagatoFill(self),
            "fit-to-width": NagatoFitToWidth(self),
            "fit-to-height": NagatoFitToHeight(self)
            }
