
from libnagatoconfig.ui.page.Page import NagatoPage
from libnagatoconfig.ui.box.selectpage.XdgUserDirs import NagatoXdgUserDirs
from libnagatoconfig.ui.box.selectpage.Appearance import NagatoAppearance
from libnagatoconfig.ui.box.selectpage.Wallpaper import NagatoWallpaper


class NagatoPortal(NagatoPage):

    HEADER_LABEL = "Main Menu"

    def _initialize_children(self):
        self.add(NagatoXdgUserDirs(self))
        self.add(NagatoAppearance(self))
        self.add(NagatoWallpaper(self))
