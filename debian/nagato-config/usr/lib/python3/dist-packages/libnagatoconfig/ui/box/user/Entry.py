
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.file import HomeDirectory
from libnagato4.widget import Margin
from libnagatoconfig.ui.box.user.EntryEvent import NagatoEntryEvent

ICON_POSITION = Gtk.EntryIconPosition.SECONDARY


class NagatoEntry(Gtk.Entry, NagatoObject):

    def _on_initialize(self):
        self.set_icon_from_icon_name(ICON_POSITION, "folder")
        self.set_icon_tooltip_markup(ICON_POSITION, "select folder")
        yuki_directory = self._enquiry("YUKI.N > directory", self._parent.KEY)
        self.set_text(HomeDirectory.shorten(yuki_directory))

    def _inform_entry(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        Gtk.Entry.__init__(self)
        self.props.editable = False
        Margin.set_with_unit(self, "margin", "margin", "margin", "margin")
        self._on_initialize()
        NagatoEntryEvent(self)
