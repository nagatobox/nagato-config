
import cairo
from gi.repository import Gdk
from libnagato4.Ux import Unit

FORMAT = cairo.Format.RGB24
WIDTH = Unit(7)
HEIGHT = Unit(3)


def new_from_rgba(rgba):
    yuki_image_surface = cairo.ImageSurface(FORMAT, WIDTH, HEIGHT)
    yuki_cairo_context = cairo.Context(yuki_image_surface)
    Gdk.cairo_set_source_rgba(yuki_cairo_context, rgba)
    yuki_cairo_context.rectangle(0, 0, WIDTH, HEIGHT)
    yuki_cairo_context.fill()
    return yuki_image_surface
