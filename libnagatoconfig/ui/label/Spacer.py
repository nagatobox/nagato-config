
from libnagatoconfig.ui.label.Label import NagatoLabel
from libnagato4.Ux import Unit


class NagatoSpacer(NagatoLabel):

    def _setup(self):
        self.set_size_request(-1, Unit("grid-spacing"))
