
from libnagatoconfig.ui.box.appearance.Appearance import NagatoAppearance
from libnagatoconfig.ui.box.user.FontButton import NagatoFontButton


class NagatoFont(NagatoAppearance):

    LABEL_TEXT = "Font"
    CSS = "config-box-odd"

    def _initialize_control(self):
        self.pack_start(NagatoFontButton(self), True, True, 0)
