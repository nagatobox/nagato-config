
from gi.repository import Gdk
from libnagato4.Object import NagatoObject
from libnagatoconfig.wallpaper.model import DesktopSession


class NagatoPattern(Gdk.RGBA, NagatoObject):

    def set_data(self, data):
        self._pattern = data

    def get_data(self):
        return self._pattern

    def __init__(self, parent):
        self._parent = parent
        yuki_query = DesktopSession.get(), "pattern", "center"
        self._pattern = self._enquiry("YUKI.N > settings", yuki_query)
