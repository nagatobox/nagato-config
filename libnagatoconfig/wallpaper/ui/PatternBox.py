
from libnagatoconfig.ui.box.Box import NagatoBox
from libnagatoconfig.wallpaper.ui.PatternButton import NagatoPatternButton


class NagatoPatternBox(NagatoBox):

    LABEL_TEXT = "Pattern"
    CSS = "config-box-even"

    def _yuki_n_add_to_box(self, widget):
        self.add(widget)

    def _initialize_control(self):
        NagatoPatternButton(self)
        self._raise("YUKI.N > add to box", self)
