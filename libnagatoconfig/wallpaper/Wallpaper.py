
from libnagatoconfig.ui.page.Page import NagatoPage
from libnagatoconfig.wallpaper.model.ModelLayer import NagatoModelLayer


class NagatoWallpaper(NagatoPage):

    HEADER_LABEL = "Wallpaper"
    ID = "wallpaper"

    def _yuki_n_add_to_box(self, widget):
        self.add(widget)

    def _initialize_children(self):
        NagatoModelLayer(self)
        self._raise("YUKI.N > add to stack named", (self, self.ID))
