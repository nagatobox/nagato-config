
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.Ux import Unit
from libnagato4.widget import Margin
from libnagatoconfig.wallpaper.ui.ColorImage import NagatoColorImage


class NagatoColorButton(Gtk.Button, NagatoObject):

    def _on_clicked(self, button):
        print("call color chooser")

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(self)
        self.set_image(NagatoColorImage(self))
        self.set_relief(Gtk.ReliefStyle.NONE)
        Margin.set_with_unit(self, "margin", "margin", "margin", "margin")
        self.set_size_request(Unit("button-width"), -1)
        self.connect("clicked", self._on_clicked)
        self._raise("YUKI.N > add to box", self)
