
from libnagatoconfig.ui.page.Page import NagatoPage
from libnagatoconfig.model.ForXdgUserDirs import NagatoModel
from libnagatoconfig.ui.box.group.XdgUserDirs import AsakuraXdgUserDirs
from libnagatoconfig.ui.label.Spacer import NagatoSpacer
from libnagatoconfig.ui.box.Apply import NagatoApply


class NagatoXdgUserDirs(NagatoPage):

    HEADER_LABEL = "XDG User Directories"

    def _inform_directory(self, key):
        return self._model[key]

    def _yuki_n_change_directory(self, user_data):
        yuki_key, yuki_value = user_data
        self._model[yuki_key] = yuki_value

    def _yuki_n_change_applicable(self, applicable):
        self._apply_box.set_sensitive(applicable)

    def _yuki_n_apply_changes(self):
        self._model.apply_changes()

    def _initialize_children(self):
        self._model = NagatoModel(self)
        AsakuraXdgUserDirs(self)
        self._apply_box = NagatoApply(self)
        self.pack_start(NagatoSpacer(self), True, False, 0)
        self.pack_start(self._apply_box, False, False, 0)
