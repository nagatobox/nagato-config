
from pathlib import Path
from libnagatoconfig.data.rc.Rc import NagatoRc


class NagatoGtk2(NagatoRc):

    PATH = Path.home().joinpath(".gtkrc-2.0")
