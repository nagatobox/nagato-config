
from libnagatoconfig.data.parser.Parser import NagatoParser


class NagatoIcons(NagatoParser):

    DIRECTORY = "/usr/share/icons"

    def _check_file(self, file_info, paths):
        if "index.theme" in paths:
            yuki_data = "icons", file_info.get_name()
            self._raise("YUKI.N > append data", yuki_data)
        if "cursor.theme" in paths:
            yuki_data = "cursors", file_info.get_name()
            self._raise("YUKI.N > append data", yuki_data)
