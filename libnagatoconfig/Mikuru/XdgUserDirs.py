
from gi.repository import GLib
from gi.repository import Gio


GLIB_INDEX = {
    "DESKTOP": 0,
    "DOCUMENTS": 1,
    "DOWNLOAD": 2,
    "MUSIC": 3,
    "PICTURES": 4,
    "PUBLICSHARE": 5,
    "TEMPLATES": 6,
    "VIDEOS": 7
}


def update(key, path):
    yuki_command = ["xdg-user-dirs-update", "--set", key, path]
    yuki_flags = Gio.SubprocessFlags.NONE
    yuki_subprocess = Gio.Subprocess.new(yuki_command, yuki_flags)
    yuki_subprocess.wait(None)


def get_data_pair():
    yuki_original_data = GLIB_INDEX.copy()
    for yuki_name, yuki_index in GLIB_INDEX.items():
        yuki_path = GLib.get_user_special_dir(yuki_index)
        yuki_original_data[yuki_name] = yuki_path
    yuki_current_data = yuki_original_data.copy()
    return yuki_original_data, yuki_current_data
