
from libnagatoconfig.ui.box.Box import NagatoBox
from libnagatoconfig.ui.button.Go import NagatoGo


class NagatoSelectPage(NagatoBox):

    BUTTON_MESSAGE = "YUKI.N > ignore", None

    def _initialize_control(self):
        self.pack_start(NagatoGo(self), False, False, 0)
