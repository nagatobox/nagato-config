
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.widget import Margin


class NagatoFontButton(Gtk.FontButton, NagatoObject):

    GTK_KEY = "gtk-font-name"

    def _on_font_set(self, font_button):
        yuki_data = self.GTK_KEY, self.get_font_name()
        self._raise("YUKI.N > change current", yuki_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.FontButton.__init__(self)
        yuki_font = self._enquiry("YUKI.N > current", self.GTK_KEY)
        self.set_font(yuki_font)
        self.set_preview_text("YUKI.N > Can you see this ?")
        Margin.set_with_unit(self, "margin", "margin", "margin", "margin")
        self.connect("font-set", self._on_font_set)
