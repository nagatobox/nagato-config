
from libnagato4.Ux import Unit
from libnagato4.mainline.MainGridLayer import AbstractMainGridLayer
from libnagatoconfig.ui.Stack import NagatoStack


class NagatoMainGridLayer(AbstractMainGridLayer):

    def _yuki_n_loopback_add_main_widget(self, parent):
        parent.set_border_width(Unit("grid-spacing"))
        parent.set_row_spacing(Unit("grid-spacing"))
        parent.set_column_spacing(Unit("grid-spacing"))
        parent.attach(NagatoStack(parent), 0, 0, 1, 1)
