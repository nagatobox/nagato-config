
from pathlib import Path
from libnagato4.Object import NagatoObject
from libnagatoconfig.rc.RcReader import NagatoRcReader
from libnagatoconfig.rc.data.Group import NagatoGroup


class NagatoRc(NagatoObject):

    PATH = "define path to rc file here."

    def _get_text(self):
        yuki_text = ""
        for yuki_key, yuki_group in self._data.items():
            if yuki_key:
                yuki_text += "[{}]\n".format(yuki_key)
            yuki_text += yuki_group.get_text()
        return yuki_text

    def _yuki_n_new_header(self, header_name):
        self._group = NagatoGroup(header_name)
        self._data[header_name] = self._group

    def _yuki_n_new_key_value(self, user_data):
        self._group.add_key_value(user_data)

    def __getitem__(self, group):
        return self._data[group]

    def save(self):
        yuki_text = self._get_text()
        Path(self.PATH).write_text(yuki_text)

    def __init__(self, parent):
        self._parent = parent
        self._data = {}
        self._group = NagatoGroup("")
        self._data[""] = self._group
        NagatoRcReader(self)
