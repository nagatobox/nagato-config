
from libnagatoconfig.ui.box.directory.Directory import NagatoDirectory


class NagatoVideos(NagatoDirectory):

    KEY = "VIDEOS"
    LABEL_TEXT = "VIDEOS"
    CSS = "config-box-even"
