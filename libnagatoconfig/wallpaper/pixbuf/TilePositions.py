

class HaruhiTilePositions:

    def _get_range(self, canvas_start, canvas_size, pixbuf_size):
        yuki_center = (canvas_size-pixbuf_size)/2
        yuki_offset = pixbuf_size-yuki_center % pixbuf_size
        yuki_start = canvas_start-yuki_offset
        yuki_end = canvas_start+canvas_size+pixbuf_size
        return range(int(yuki_start), int(yuki_end), int(pixbuf_size))

    def _get_x_range(self, geometry, pixbuf):
        yuki_canvas_x, _, yuki_canvas_w, _ = geometry
        yuki_pixbuf_w = pixbuf.get_width()
        return self._get_range(yuki_canvas_x, yuki_canvas_w, yuki_pixbuf_w)

    def _get_y_range(self, geometry, pixbuf):
        _, yuki_canvas_y, _, yuki_canvas_h = geometry
        yuki_pixbuf_h = pixbuf.get_width()
        return self._get_range(yuki_canvas_y, yuki_canvas_h, yuki_pixbuf_h)

    def get(self, geometry, pixbuf):
        for yuki_x in self._get_x_range(geometry, pixbuf):
            for yuki_y in self._get_y_range(geometry, pixbuf):
                yield yuki_x, yuki_y
