
from libnagatoconfig.wallpaper.painter.CheckerBoard import NagatoCheckerBoard
from libnagatoconfig.wallpaper.painter.Background import NagatoBackground


class AsakuraPainters:

    def paint(self, cairo_context):
        self._checker_board.paint(cairo_context)
        self._background.paint(cairo_context)

    def __init__(self, parent):
        self._checker_board = NagatoCheckerBoard(parent)
        self._background = NagatoBackground(parent)
