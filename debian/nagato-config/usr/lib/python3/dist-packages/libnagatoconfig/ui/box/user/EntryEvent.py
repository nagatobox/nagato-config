
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.file import HomeDirectory
from libnagato4.chooser.context.SelectDirectory import NagatoContext
from libnagato4.chooser.selectfile.Dialog import NagatoDialog


class NagatoEntryEvent(NagatoObject):

    def _on_icon_press(self, entry, icon_position, event, key):
        yuki_current = self._enquiry("YUKI.N > directory", key)
        yuki_context = NagatoContext.new_for_directory(self)
        yuki_response = NagatoDialog.run_with_context(self, yuki_context)
        if yuki_response != Gtk.ResponseType.APPLY:
            return
        yuki_selected = yuki_context["selection"]
        if yuki_current != yuki_selected:
            entry.set_text(HomeDirectory.shorten(yuki_selected))
            self._raise("YUKI.N > change directory", (key, yuki_selected))

    def __init__(self, parent):
        self._parent = parent
        yuki_entry = self._enquiry("YUKI.N > entry")
        yuki_key = self._enquiry("YUKI.N > key")
        yuki_entry.connect("icon-press", self._on_icon_press, yuki_key)
