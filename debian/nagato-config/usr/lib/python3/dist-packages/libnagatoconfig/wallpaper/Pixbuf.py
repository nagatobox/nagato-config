
from gi.repository import GdkPixbuf
from libnagato4.Object import NagatoObject
from libnagatoconfig.wallpaper.PixbufPaths import NagatoPixbufPaths


class NagatoPixbuf(NagatoObject):

    def __init__(self, parent):
        self._parent = parent
        self._source_pixbuf = None
        self._paths = NagatoPixbufPaths(self)
        if (yuki_path := self._paths.get_path("source.png")) is not None:
            self._source_pixbuf = GdkPixbuf.Pixbuf.new_from_file(yuki_path)

