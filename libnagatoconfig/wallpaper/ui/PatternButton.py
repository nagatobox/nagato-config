
from gi.repository import Gtk
from libnagato4.widget import Margin
from libnagato4.Ux import Unit
from libnagato4.Object import NagatoObject
from libnagatoconfig.wallpaper.popover.Popover import NagatoPopover


class NagatoPatternButton(Gtk.Button, NagatoObject):

    def _refresh(self):
        yuki_pattern = self._enquiry("YUKI.N > wallpaper config", "pattern")
        self.set_label(yuki_pattern)

    def _on_clicked(self, button):
        self._popover.set_relative_to(button)
        self._popover.popup()
        self._popover.show_all()

    def receive_transmission(self, user_data):
        self._refresh()

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(self)
        self.set_relief(Gtk.ReliefStyle.NONE)
        Margin.set_with_unit(self, "margin", "margin", "margin", "margin")
        self.set_size_request(Unit("button-width"), -1)
        self.connect("clicked", self._on_clicked)
        self._raise("YUKI.N > add to box", self)
        self._popover = NagatoPopover(self)
        self._refresh()
        self._raise("YUKI.N > register model object", self)
