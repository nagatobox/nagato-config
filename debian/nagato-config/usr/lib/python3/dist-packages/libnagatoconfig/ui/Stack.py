
from libnagato4.Object import NagatoObject
from gi.repository import Gtk
from libnagatoconfig.ui.page.Portal import NagatoPortal
from libnagatoconfig.ui.page.Appearance import NagatoAppearance
from libnagatoconfig.ui.page.XdgUserDirs import NagatoXdgUserDirs
from libnagatoconfig.wallpaper.Wallpaper import NagatoWallpaper


class NagatoStack(Gtk.Stack, NagatoObject):

    def _yuki_n_page_move_to(self, page_name):
        self.set_visible_child_name(page_name)

    def _yuki_n_add_to_stack_named(self, user_data):
        yuki_page, yuki_name = user_data
        self.add_named(yuki_page, yuki_name)

    def _initialize_children(self):
        self.add_named(NagatoXdgUserDirs(self), "xdg-user-dirs")
        self.add_named(NagatoAppearance(self), "appearance")
        NagatoWallpaper(self)
        self.add_named(NagatoPortal(self), "portal")

    def __init__(self, parent):
        self._parent = parent
        Gtk.Stack.__init__(self)
        self.set_transition_type(Gtk.StackTransitionType.OVER_RIGHT_LEFT)
        self._initialize_children()
        self.set_visible_child_name("portal")
